package me.thlim.client;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;

/**
 *
 * @author: Lim, Teck Hooi
 *
 * Date: 6/23/12
 * Time: 8:30 PM
 *
 */
public class SpringLoadedOrderProducer
{
    private static final Logger LOGGER = LoggerFactory.getLogger(SpringLoadedOrderProducer.class);


    @Autowired
    private JmsTemplate jmsTemplate;

    private int maxMessages = 50;
    private int waitPeriod = 4;
    private int maxProducers = 10;
    private ExecutorService producerPool;

    public void initProducerPool()
    {
        producerPool = Executors.newFixedThreadPool(maxProducers);
    }

    public void fireMessages() throws Exception
    {

        for (int i = 0; i < maxMessages; i++)
        {
            final int j = i;
            producerPool.submit(new Runnable()
            {
                @Override
                public void run()
                {
                    try
                    {
                        jmsTemplate.convertAndSend("Message " + j);
                        LOGGER.debug("Sent message " + j);
                    }
                    catch (Exception e)
                    {
                        LOGGER.warn("Failed to send message " + j, e);
                    }
                }
            });
        }
    }

    public void closeProducerPool()
    {
        producerPool.shutdown();

        LOGGER.info("Services shutdown.");

        // Give another 10s for thread pool to shutdown.
        try
        {
            if (producerPool.awaitTermination(waitPeriod, TimeUnit.SECONDS))
            {
                LOGGER.info("Thread pool shutdown.");
            }
            else
            {
                LOGGER.info("Thread pool NOT shutdown.");
            }
        }
        catch (InterruptedException e)
        {
            LOGGER.warn("Interrupted while shuting down producer pool.", e);
        }
    }

    public int getMaxMessages()
    {
        return maxMessages;
    }

    public int getWaitPeriod()
    {
        return waitPeriod;
    }

    public int getMaxProducers()
    {
        return maxProducers;
    }

    public void setMaxMessages(int maxMessages)
    {
        this.maxMessages = maxMessages;
    }

    public void setWaitPeriod(int waitPeriod)
    {
        this.waitPeriod = waitPeriod;
    }

    public void setMaxProducers(int maxProducers)
    {
        this.maxProducers = maxProducers;
    }
}
