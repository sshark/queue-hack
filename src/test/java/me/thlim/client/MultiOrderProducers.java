package me.thlim.client;

import java.util.Hashtable;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 *
 * @author: Lim, Teck Hooi
 *
 * Date: 6/23/12
 * Time: 8:30 PM
 *
 */
public class MultiOrderProducers implements ApplicationContextAware
{
    private static final Logger LOGGER = LoggerFactory.getLogger(MultiOrderProducers.class);
    private final String OK = "SUCCESSFUL";
    private final String FAILED = "FAILED";

    private final ResourceBundle bundle = ResourceBundle.getBundle("queue");
    private final String JNDI_FACTORY_KEY = "queue.server.jndi.factory";
    private final String JMS_FACTORY_KEY = "queue.server.jms.factory";
    private final String Q_SERVER_URL_KEY = "queue.server.url";
    private final String Q_SERVER_CREDENTIAL_KEY = "queue.server.credentials";
    private final String Q_SERVER_PRINCIPAL_KEY = "queue.server.principal";
    private final String ORDER_RESULT_Q = "queue.example.name";

    private QueueConnection connection;
    private Context context;
    private String queueName;

    public MultiOrderProducers() throws NamingException, JMSException
    {
        init();
        queueName = bundle.getString(ORDER_RESULT_Q);
    }

    public MultiOrderProducers(String queueName) throws NamingException, JMSException
    {
        init();
        this.queueName = queueName;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException
    {
    }

    private void init() throws NamingException, JMSException
    {
        Hashtable env = new Hashtable();
        env.put(Context.INITIAL_CONTEXT_FACTORY, bundle.getString(JNDI_FACTORY_KEY));
        env.put(Context.PROVIDER_URL, bundle.getString(Q_SERVER_URL_KEY));
        env.put(Context.SECURITY_PRINCIPAL, bundle.getString(Q_SERVER_PRINCIPAL_KEY));
        env.put(Context.SECURITY_CREDENTIALS, bundle.getString(Q_SERVER_CREDENTIAL_KEY));

        context = new InitialContext(env);
        QueueConnectionFactory factory = (QueueConnectionFactory)
            context.lookup(bundle.getString(JMS_FACTORY_KEY));
        connection = factory.createQueueConnection(
            bundle.getString(Q_SERVER_PRINCIPAL_KEY),
            bundle.getString(Q_SERVER_CREDENTIAL_KEY));
    }

    public void sendMessage(String message) throws Exception
    {
        QueueSession session = connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
        MessageProducer producer = session.createProducer((Destination)context.lookup(queueName));
        TextMessage messageHolder = session.createTextMessage();
        messageHolder.setText(message);

        producer.send(messageHolder);
    }

    public void closeConnection()
    {
        try
        {
            connection.close();
            LOGGER.info("Connection closed.");
        }
        catch (Exception e)
        {
            LOGGER.warn("Failed to close connection.", e);
        }

    }

    public static void main(String[] args) throws Exception
    {
        ExecutorService threadPool = Executors.newFixedThreadPool(50);
        final MultiOrderProducers orderProducer = new MultiOrderProducers();

        for (int i = 0; i < 50; i++)
        {
            final int j = i;
            threadPool.submit(new Runnable()
            {
                @Override
                public void run()
                {
                    try
                    {
                        orderProducer.sendMessage("Message " + j);
                        LOGGER.debug("Sent message " + j);
                    }
                    catch (Exception e)
                    {
                        LOGGER.warn("Failed to send message " + j, e);
                    }
                }
            });
        }
        threadPool.shutdown();
        threadPool.awaitTermination(60, TimeUnit.SECONDS);
        orderProducer.sendMessage("QUIT");
        orderProducer.closeConnection();
        LOGGER.info("Sent all messages. Done.");
    }
}
