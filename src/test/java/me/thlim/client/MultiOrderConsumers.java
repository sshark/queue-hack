package me.thlim.client;

import java.util.Hashtable;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This application launches multiple consumers with single connection. Connections are thread safe but sessions
 * are not.
 *
 * @author: Lim, Teck Hooi
 *
 * Date: 7/2/12
 * Time: 4:34 PM
 *
 */
public class MultiOrderConsumers
{
    private static final Logger LOGGER = LoggerFactory.getLogger(MultiOrderConsumers.class);

    private final String JNDI_FACTORY_KEY = "queue.server.jndi.factory";
    private final String JMS_FACTORY_KEY = "queue.server.jms.factory";
    private final String Q_SERVER_URL_KEY = "queue.server.url";
    private final String Q_SERVER_CREDENTIAL_KEY = "queue.server.credentials";
    private final String Q_SERVER_PRINCIPAL_KEY = "queue.server.principal";
    private boolean shutdown = false;
    private int messageCount = 1;
    private QueueConnectionFactory factory = null;
    private Context context = null;
    private ResourceBundle bundle;
    private ExecutorService threadPool;
    private boolean remoteShutDownAllowed = false;
    private int numConsumers;

    public MultiOrderConsumers(int numConsumers) throws NamingException, JMSException
    {
        this(numConsumers, false);
    }

    public MultiOrderConsumers(int numConsumers, boolean remoteShutDownAllowed) throws NamingException, JMSException
    {
        this("queue", numConsumers, remoteShutDownAllowed);
    }

    public MultiOrderConsumers(String bundleName, int numConsumers, boolean remoteShutDownAllowed) throws NamingException, JMSException
    {
        this.remoteShutDownAllowed = remoteShutDownAllowed;
        this.numConsumers = numConsumers;
        bundle = ResourceBundle.getBundle(bundleName);
        kickoffConnection(bundle);
        LOGGER.info("Listener initialized.");
    }

    private void kickoffConnection(ResourceBundle bundle) throws NamingException, JMSException
    {
        context = getContext(bundle);
        factory = (QueueConnectionFactory)
        context.lookup(bundle.getString(JMS_FACTORY_KEY));
    }

    public void kickoffConsumer(String queueName) throws JMSException, NamingException
    {
        QueueConnection connection = factory.createQueueConnection(
            bundle.getString(Q_SERVER_PRINCIPAL_KEY),
            bundle.getString(Q_SERVER_CREDENTIAL_KEY));

        connection.start();

        ExecutorService workersThreadPool = Executors.newFixedThreadPool(numConsumers);

        for (int i = 0; i < numConsumers; i++)
        {
            QueueSession session = connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
            MessageConsumer consumer = session.createConsumer((Destination)context.lookup(queueName));
            workersThreadPool.submit(new ConsumerWorker(consumer));
        }

        LOGGER.info("Listener started.");

        while (!shutdown) {}
        LOGGER.info("Collected " + messageCount + " messages.");
        close(connection);
    }

    private void close(QueueConnection connection)
    {
        try
        {
            connection.close();
        }
        catch (JMSException e)
        {
            LOGGER.warn("Failed to closeConnection connection", e);
        }
    }

    public void shutDown()
    {
        shutdown = true;
        threadPool.shutdown();
        try
        {
            threadPool.awaitTermination(60, TimeUnit.SECONDS);
        }
        catch (InterruptedException e)
        {
            LOGGER.warn("Interruption while waiting for thread pool to terminate.", e);
        }
        if (!threadPool.isShutdown())
        {
            threadPool.shutdownNow();
            LOGGER.warn("Thread pool forced shutdown.");
        }
    }

    protected Context getContext(ResourceBundle bundle) throws NamingException, JMSException
    {
        Hashtable env = new Hashtable();
        env.put(Context.INITIAL_CONTEXT_FACTORY, bundle.getString(JNDI_FACTORY_KEY));
        env.put(Context.PROVIDER_URL, bundle.getString(Q_SERVER_URL_KEY));
        env.put(Context.SECURITY_PRINCIPAL, bundle.getString(Q_SERVER_PRINCIPAL_KEY));
        env.put(Context.SECURITY_CREDENTIALS, bundle.getString(Q_SERVER_CREDENTIAL_KEY));

        return new InitialContext(env);
    }

    class ConsumerWorker implements Runnable
    {
        private MessageConsumer consumer;

        @Override
        public void run()
        {
            while (!shutdown)
            {
                Message message = null;
                try
                {
                    message = consumer.receive(2000);
                    if (message != null)
                    {
                        if (isShutdownCodeReceived(message))
                        {
                            LOGGER.warn("Shutdown message received.");
                            shutDown();
                        }
                        else
                        {
                            onMessage(message);
                        }
                    }
                }
                catch (JMSException e)
                {
                    e.printStackTrace();
                }
            }
        }

        protected boolean isShutdownCodeReceived(Message shutDownMessage) throws JMSException
        {
            return remoteShutDownAllowed &&
                shutDownMessage != null &&
                shutDownMessage instanceof TextMessage &&
                "QUIT".equals(((TextMessage)shutDownMessage).getText());
        }

        public ConsumerWorker(MessageConsumer consumer)
        {
            this.consumer = consumer;
        }

        public void onMessage(final Message message)
        {
            try
            {
                if (message instanceof TextMessage)
                {
                    String textReceived = ((TextMessage)message).getText();
                    LOGGER.debug(messageCount + ". Received " + textReceived);
                    try
                    {
                        Thread.sleep(2000);
                    }
                    catch (InterruptedException e)
                    {
                        // Stimulate heavy process
                    }
                    messageCount++;
                }
            }
            catch (JMSException e)
            {
                LOGGER.error("Failed to receive message.", e);
            }
        }
    }

    public static void main(String[] args) throws NamingException, JMSException
    {
        MultiOrderConsumers consumer = new MultiOrderConsumers(10);
        consumer.kickoffConsumer("test/exampleQueue");
    }
}
