package me.thlim.client;

import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author: Lim, Teck Hooi
 *
 * Date: 6/23/12
 * Time: 8:30 PM
 *
 */
public class SpringLoadedJMSIntegrationTest
{
    private static final Logger LOGGER = LoggerFactory.getLogger(SpringLoadedJMSIntegrationTest.class);

    private final int WAIT_10_SECONDS = 10000;

    private ClassPathXmlApplicationContext applicationContext;

    @Before
    public void setup()
    {
        applicationContext = new ClassPathXmlApplicationContext("applicationContext-jboss.xml");
    }

    @After
    public void done()
    {
        applicationContext.close();
    }

    @Test
    public void testMultipleProducersAndConsumers() throws Exception
    {

        final SpringLoadedOrderProducer producer = (SpringLoadedOrderProducer)applicationContext.getBean("orderProducer");

        producer.initProducerPool();
        producer.fireMessages();

        // Wait for consumers to receive all the messages produced
        try
        {
            Thread.sleep(WAIT_10_SECONDS);
        }
        catch (InterruptedException e)
        {
            // NOTHING
        }

        producer.closeProducerPool();

        LOGGER.debug("Messages received are " + SpringLoadedOrderConsumer.getTotalMessageReceived());
        Assert.assertEquals("Messages received do not tally.", producer.getMaxMessages(), SpringLoadedOrderConsumer.getTotalMessageReceived());
    }
}
