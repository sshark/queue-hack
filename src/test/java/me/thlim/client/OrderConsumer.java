package me.thlim.client;

import java.util.Hashtable;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author: Lim, Teck Hooi
 *
 * Date: 7/2/12
 * Time: 4:34 PM
 *
 */
public class OrderConsumer implements MessageListener
{
    private static final Logger LOGGER = LoggerFactory.getLogger(OrderConsumer.class);

    private final String OK = "SUCCESSFUL";
    private final String FAILED = "FAILED";
    private final String JNDI_FACTORY_KEY = "queue.server.jndi.factory";
    private final String JMS_FACTORY_KEY = "queue.server.jms.factory";
    private final String Q_SERVER_URL_KEY = "queue.server.url";
    private final String Q_SERVER_CREDENTIAL_KEY = "queue.server.credentials";
    private final String Q_SERVER_PRINCIPAL_KEY = "queue.server.principal";
    private final String ORDER_RESULT_Q = "queue.receiver.name.order";
    private final String RECEIVER_SLEEP_MS = "queue.receiver.sleep.ms";
    private boolean shutDown = false;
    private int messageCount = 1;
    private Random random = new Random();
    private ExecutorService threadPool;


    public OrderConsumer()
    {
        LOGGER.info("Listener initialized.");
        threadPool = Executors.newFixedThreadPool(50);
        ResourceBundle bundle = ResourceBundle.getBundle("queue");
        try
        {
            kickoffConsumer(bundle);
        }
        catch (ServletException e)
        {
            LOGGER.error("Failed to start consumer.", e);
        }
    }


    private void kickoffConsumer(ResourceBundle bundle) throws ServletException
    {
        Context context = null;
        try
        {
            context = initContext(bundle);
        }
        catch (Exception e)
        {
            throw new ServletException("Failed to init context.", e);
        }

        QueueConnectionFactory factory = null;
        QueueConnection connection = null;
        try
        {
            factory = (QueueConnectionFactory)
                context.lookup(bundle.getString(JMS_FACTORY_KEY));
            connection = factory.createQueueConnection(bundle.getString(Q_SERVER_PRINCIPAL_KEY),
                bundle.getString(Q_SERVER_CREDENTIAL_KEY));
        }
        catch (NamingException e)
        {
            throw new ServletException("Failed to lookup for factory.", e);
        }
        catch (JMSException e)
        {
            throw new ServletException("Failed to create queue connection.");
        }

        try
        {
            connection.start();
        }
        catch (JMSException e)
        {
            throw new ServletException("Failed to start connection");
        }

        QueueSession session = null;
        try
        {
            session = connection.createQueueSession(false, Session.CLIENT_ACKNOWLEDGE);
        }
        catch (JMSException e)
        {
            throw new ServletException("Failed to create session.", e);
        }

        MessageConsumer consumer = null;
        try
        {
            consumer = session.createConsumer((Destination)context.lookup(bundle.getString("queue.example.name")));
            consumer.setMessageListener(this);
        }
        catch (Exception e)
        {
            throw new ServletException("Failed to create consumer or add listener to consumer.", e);
        }

        LOGGER.info("Listener started.");
        while (!shutDown)
        {
            try
            {
                Thread.sleep(6000);
            }
            catch (InterruptedException e)
            {
                // NOTHING TO DO
            }
            LOGGER.debug("Shutting down now? " + (shutDown ? "Yes" : "No"));
        }
        LOGGER.info("Collected " + messageCount + " messages.");
        close(session, connection);


        /*
        LOGGER.info("Listener started.");
        int messageCount = 1;
        while (!shutDown)
        {
            try
            {
                Message message = consumer.receive(2000);
                if (message != null && message instanceof TextMessage)
                {
                    LOGGER.debug(messageCount + ". Received message : " + ((TextMessage)message).getText());
                    messageCount++;
                }
            }
            catch (JMSException e)
            {
                closeConnection(session, connection);
                throw new ServletException("Failed to receive message.", e);
            }
        }
        LOGGER.info("Collected " + messageCount + " messages. Listener shutdown.");
        closeConnection(session, connection);
        */
    }

    private void close(QueueSession session, QueueConnection connection)
    {
        try
        {
            connection.close();
        }
        catch (JMSException e)
        {
            LOGGER.warn("Failed to closeConnection connection", e);
        }
    }

    private Context initContext(ResourceBundle bundle) throws NamingException, JMSException
    {
        Hashtable env = new Hashtable();
        env.put(Context.INITIAL_CONTEXT_FACTORY, bundle.getString(JNDI_FACTORY_KEY));
        env.put(Context.PROVIDER_URL, bundle.getString(Q_SERVER_URL_KEY));
        env.put(Context.SECURITY_PRINCIPAL, bundle.getString(Q_SERVER_PRINCIPAL_KEY));
        env.put(Context.SECURITY_CREDENTIALS, bundle.getString(Q_SERVER_CREDENTIAL_KEY));

        return new InitialContext(env);
    }

    /* with thread pool
    @Override
    public void onMessage(final Message message)
    {
        threadPool.submit(new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    if (message != null && message instanceof TextMessage)
                    {
                        String textReceived = ((TextMessage)message).getText();
                        if ("QUIT".equals(textReceived))
                        {
                            shutDown = true;
                            return;
                        }
                        LOGGER.debug(messageCount + ". Received " + textReceived);
                        messageCount++;
                    }
                }
                catch (JMSException e)
                {
                    LOGGER.error("Failed to receive message.", e);
                }
            }
        });
    }
    */

    @Override
    public void onMessage(final Message message)
    {
        try
        {
            if (message != null && message instanceof TextMessage)
            {
                String textReceived = ((TextMessage)message).getText();
                if ("QUIT".equals(textReceived))
                {
                    shutDown = true;
                    return;
                }
                LOGGER.debug(messageCount + ". Received " + textReceived);
                messageCount++;
            }
        }
        catch (JMSException e)
        {
            LOGGER.error("Failed to receive message.", e);
        }
    }

    public static void main(String[] args) throws NamingException, JMSException
    {
        new OrderConsumer();
    }
}
