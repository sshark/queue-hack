package me.thlim.client;

import java.util.concurrent.atomic.AtomicInteger;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;

/**
 * This application launches multiple consumers with single connection. Connections are thread safe but sessions
 * are not.
 *
 * @author: Lim, Teck Hooi
 *
 * Date: 7/2/12
 * Time: 4:34 PM
 *
 */
public class SpringLoadedOrderConsumer implements MessageListener
{
    private static final Logger LOGGER = LoggerFactory.getLogger(SpringLoadedOrderConsumer.class);

    @Autowired
    private JmsTemplate jmsTemplate;

    static private AtomicInteger counter = new AtomicInteger();

    @Override
    public void onMessage(Message message)
    {
        //GlobalCounter counter = GlobalCounter.getInstance();

        try
        {
            if (message instanceof TextMessage)
            {
                String textReceived = ((TextMessage)message).getText();

                /*
                try
                {
                    Thread.sleep(2000);
                }
                catch (InterruptedException e)
                {
                    // Stimulate heavy process
                }
                */

                LOGGER.debug(counter.incrementAndGet() + ". Received " + textReceived);
            }
        }
        catch (JMSException e)
        {
            LOGGER.error("Failed to receive message.", e);
        }
    }

    static public int getTotalMessageReceived()
    {
        return counter.get();
    }
}
